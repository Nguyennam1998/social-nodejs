// out side
const express = require('express')
const compress = require('compression')
const cors = require('cors')
const cookieParser = require('cookie-parser')
const helmet = require('helmet')
const bodyParser = require('body-parser')
const swaggerUI = require('swagger-ui-express')
const swaggerJsDoc = require('swagger-jsdoc')
//inside
const config = require('./config')
const userRouter = require('./routes/user.routes')
const authRouter = require('./routes/auth.routes')
const mongoose = require('mongoose')
const app = express()
const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'social app',
      version: '1.0.0',
      description: 'learning nodejs mogodb',
    },
    servers: [
      {
        url: 'http://localhost:4000',
      },
    ],
  },
  apis: ['./routes/*.js'],
}

const specs = swaggerJsDoc(options)

app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(specs))

app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true }))
app.use(compress())
app.use(helmet())
app.use(cors())
app.use((err, req, res, next) => {
  if (err.name === 'UnauthorizedError') {
    res.status(401).json({"error" : err.name + ": " + err.message})
  }else if (err) {
    res.status(400).json({"error" : err.name + ": " + err.message})
    console.log(err)
  }
})
// connect db
mongoose.Promise = global.Promise
mongoose.connect(config.mongoUri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
mongoose.connection.on('error', () => {
  throw new Error(`unable to connect to database: ${config.mongoUri}`)
})
// router
app.get('/', (req, res) => {
  res.send('heloo world')
})
app.use('/', userRouter)
app.use('/', authRouter)
app.listen(config.port, () => {
  console.log(`Example app listening at http://localhost:${config.port}`)
})
