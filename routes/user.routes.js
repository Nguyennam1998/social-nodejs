const express = require('express')
const router = express.Router()
const userCtrl = require('../controllers/user.controller')
const authCtrl = require('../controllers/auth.controller')

router.route('/api/users').get(userCtrl.list).post(userCtrl.create)
router
  .route('/api/users/:userID')
  .get(authCtrl.requireSignin, userCtrl.read)
  .put(authCtrl.requireSignin, userCtrl.update)
  .delete(authCtrl.requireSignin, userCtrl.remove)
router.param('userId', userCtrl.userByID)

/**
 * @swagger
 * components:
 *   schemas:
 *     User:
 *       type: json
 *       required:
 *         - name
 *         - email
 *         - password
 *       properties:
 *         name:
 *           type: string
 *           description: Name of user
 *         email:
 *           type: string
 *           description: Email of user
 *         salt:
 *           type: string
 *           description: salt
 *         password:
 *           type: string
 *           description: password
 *         created:
 *           type: string
 *           description : Time created account
 *         updated:
 *           type: string
 *           description : Time updated account
 *
 *       example:
 *         name: nguyen van a
 *         email: nguyena@gmail.com
 *         hashed_password: qtqtqtqt1515
 */

/**
 * @swagger
 * tags:
 *   name: Users
 *   description: Users of social app
 */

/**
 * @swagger
 * /api/users:
 *   post:
 *     summary: Create a JSONPlaceholder user.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: The user's name.
 *                 example: test123
 *               email:
 *                 type: string
 *                 description: The user's email.
 *                 example: test123@gmail.com
 *               password:
 *                 type: string
 *                 description: The user's password.
 *                 example: 12345678
 *     responses:
 *       200:
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 message:
 *                   type: string
 *                   example: succesfully signed up!
*/
/**
 * @swagger
 * /api/users:
 *   get:
 *     summary: Create a JSONPlaceholder user.
 *     responses:
 *       200:
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                   type: object
 *                   properties:
 *                     id:
 *                       type: string
 *                       description: The user ID.
 *                       example: id123445
 *                     name:
 *                       type: string
 *                       description: The user name.
 *                       example: test123
 *                     email:
 *                       type: string
 *                       description: The user email.
 *                       example: test123@gmail.com
 *                     created:
 *                       type: Date
 *                       description: The user .
 *                       example: 
*/
module.exports = router
